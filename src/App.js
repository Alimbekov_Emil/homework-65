import React from "react";
import { Route, Switch } from "react-router-dom";
import Layout from "./Components/Layout/Layout";
import AdminPage from "./Container/AdminPage/AdminPage";
import HomePage from "./Container/HomePage/HomePage";
import "./App.css";

const App = () => (
  <Layout>
    <div className="container">
      <Switch>
        <Route path="/pages/admin" exact component={AdminPage} />
        <Route path="/pages/:id" exact component={HomePage} />
        <Route render={() => <h1>404 Not Found</h1>} />
      </Switch>
    </div>
  </Layout>
);

export default App;
