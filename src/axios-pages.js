import axios from "axios";

const axiosPage = axios.create({
  baseURL: "https://alimbekov-class-work-63-default-rtdb.firebaseio.com/",
});

export default axiosPage;
