import React from "react";
import NavigationsItems from "../NavigationItems/NavigationItems";
import "./ToolBar.css";

const ToolBar = () => {
  return (
    <header className="Toolbar">
      <p>Static Pages</p>
      <NavigationsItems />
    </header>
  );
};

export default ToolBar;
