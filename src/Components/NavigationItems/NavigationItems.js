import React from "react";
import "./NavigationItems.css";
import NavigationItem from "./NavigationItem/NavigationItem";
import { PAGE } from "../../constans";
import { NavLink } from "react-router-dom";

const NavigationsItems = () => {
  return (
    <ul className="NavigationItems">
      {PAGE.map((name) => {
        return (
          <NavigationItem key={name.id} to={name.id}>
            {name.title}
          </NavigationItem>
        );
      })}
      <li className="NavigationItem">
        <NavLink to="/pages/admin">Admin</NavLink>
      </li>
    </ul>
  );
};

export default NavigationsItems;
