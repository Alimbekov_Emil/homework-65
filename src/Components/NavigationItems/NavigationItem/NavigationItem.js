import React from "react";
import "./NavigationItem.css";
import { NavLink } from "react-router-dom";

const NavigationItem = ({ to, children }) => (
  <li className="NavigationItem">
    <NavLink to={"/pages/" + to}>{children}</NavLink>
  </li>
);

export default NavigationItem;
