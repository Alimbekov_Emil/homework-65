import React from "react";
import "./Page.css";

const Page = (props) => {
  return (
    <div className="Page">
      <h1>{props.title}</h1>
      <p>{props.content}</p>
    </div>
  );
};

export default Page;
