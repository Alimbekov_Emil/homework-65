import React, { useEffect, useState } from "react";
import axiosPage from "../../axios-pages";
import Page from "../../Components/Page/Page";

const HomePage = (props) => {
  const id = props.match.params.id;
  const [page, setPage] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const array = [];
      const response = await axiosPage.get("pages/" + id + ".json");
      array.push({ ...response.data, id });
      setPage(array);
    };
    fetchData().catch(console.error);
  }, [id]);

  const pages = page.map((page) => {
    return <Page key={page.id} title={page.title} content={page.content} />;
  });

  return <>{pages}</>;
};

export default HomePage;
