import React, { useEffect, useState } from "react";
import axiosPage from "../../axios-pages";
import { PAGE } from "../../constans";
import "./AdminPage.css";

const AdminPage = (props) => {
  const [page, setPage] = useState({
    title: "",
    content: "",
    option: PAGE[0].id,
  });

  const quoteDataChanged = (event) => {
    const name = event.target.name;
    const value = event.target.value;

    setPage((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  const option = PAGE.map((page) => {
    return (
      <option key={page.id} value={page.id}>
        {page.title}
      </option>
    );
  });

  useEffect(() => {
    const fetchData = async () => {
      const response = await axiosPage.get("/pages/" + page.option + ".json");
      setPage({ ...response.data, option: page.option });
    };
    fetchData().catch(console.error);
  }, [page.option]);

  const postHandler = async (event) => {
    event.preventDefault();
    const pages = { title: page.title, content: page.content };
    
    try {
      await axiosPage.put("/pages/" + page.option + ".json", pages);
    } finally {
      props.history.push("/pages/" + page.option);
    }
  };
  return (
    <>
      <form className="AdminPage" onSubmit={postHandler}>
        <h3>Edit Your Pages</h3>
        <label>
          Page
          <select name="option" onChange={quoteDataChanged}>
            {option}
          </select>
        </label>
        <label>
          Title
          <input
            type="text"
            name="title"
            className="Field"
            onChange={quoteDataChanged}
            value={page.title}
          />
        </label>
        <label>
          Content
          <textarea
            type="text"
            name="content"
            cols="100"
            rows="10"
            className="Field"
            onChange={quoteDataChanged}
            value={page.content}
          />
        </label>
        <button>Save</button>
      </form>
    </>
  );
};

export default AdminPage;
